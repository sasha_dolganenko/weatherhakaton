package com.example.user.weather;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.weather.API.Retrofit;
import com.example.user.weather.DataModel.Data;
import com.example.user.weather.DataModel.MyResponse;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity {

    TextView tv1;
    TextView tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv1 = (TextView) findViewById(R.id.id1);
        tv2 = (TextView) findViewById(R.id.id2);

        Retrofit.getAllWeather("Kharkiv", "json", "5", "eeec84ddb03248e9d60659d27376f", new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
                tv1.setText(myResponse.data.ClimateAverages[0].month[0].name.toString());
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
